$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items:1,
        loop:true,
        responsiveClass:true,
        nav:true,
        dots:true,
        dotsEach:true,
        dotClass:'fas fa-circle'
    });
  });

var input = document.querySelector('.header__search-input');
var menu_toggler = document.querySelector('.header > i');
var menu = document.querySelector('.header .container');

document.querySelector('.header__search .menu__link').addEventListener('click', function (e) {
    input.style.display = "inline-block";
    e.stopPropagation();
});

input.addEventListener('blur', function(e){
    e.target.style.display = "none";
});

input.addEventListener('click', function(e){
    e.stopPropagation();
});

document.addEventListener('click', function(e){
    if (input.style.display == "inline-block") {
       input.style.display = "none";
    }
});

menu_toggler.addEventListener('click', function(){
    if (menu.style.height == "") {
        menu.style.height = "100%";
    }
    else{
        menu.style.height = "";
    }
});
